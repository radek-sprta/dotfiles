# Import aliases
source ~/.config/fish/aliases.fish

# Set defaults
set -xU BROWSER firefox
set -xU EDITOR nvim
set -xU XFG_CONFIG_HOME $HOME/.config

# Make less more useful
set -g -x LESS '--quit-if-one-screen --ignore-case --LONG-PROMPT --RAW-CONTROL-CHARS --HILITE-UNREAD --tabs=4 --no-init --window=-4'

# Colors for less
set -xU LESS_TERMCAP_md (printf "\e[01;31m")
set -xU LESS_TERMCAP_me (printf "\e[0m")
set -xU LESS_TERMCAP_se (printf "\e[0m")
set -xU LESS_TERMCAP_so (printf "\e[01;44;33m")
set -xU LESS_TERMCAP_ue (printf "\e[0m")
set -xU LESS_TERMCAP_us (printf "\e[01;32m")
if type -q lesspipe.sh
    set -g -x LESSOPEN '|lesspipe.sh %s'
end
if type -q pygmentize
      set -xU LESSCOLORIZER 'pygmentize'
end

# Add pipx to path
set -x PATH $PATH /home/dak/.local/bin

# Add cargo to path
set -x PATH $PATH /home/dak/.cargo/bin

# Enable vi mode
fish_vi_key_bindings

# Remove fish greeting
set fish_greeting ""

# Init mcfly
if type -q mcfly
    mcfly init fish | source
end

# Init cdup
if type -q cdup
    cdup init fish | source
end

# Init zoxide
if type -q zoxide
    zoxide init --cmd to fish | source
end

# Init starship
if type -q starship
    starship init fish | source
    enable_transience
end
