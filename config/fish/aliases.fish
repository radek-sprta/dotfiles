# App overrides
alias cat='bat --style=plain'
alias vim=nvim

alias d=docker
alias g=git
alias j=just
alias k=kubectl
alias ot=opentofu
alias tf=terraform

alias la='lsd -a'
alias ll='lsd -l'
alias lla='lsd -la'
alias ls='lsd -1'
alias lt='lsd --tree'

abbr -a kns kubie ns
abbr -a kctx kubie ctx
