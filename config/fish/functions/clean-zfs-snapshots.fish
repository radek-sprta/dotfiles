function clean-zfs-snapshots -d "Remove autozsys snapshots"
    for ss in (zfs list -t snapshot -o name | grep autozsys)
        echo "Removing $ss"
        sudo zfs destroy $ss
    end
end
