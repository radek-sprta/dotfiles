function sudo --description "Make sudo without an argument run the previous command"
    if test -z "$argv"
        eval command sudo $history[1]
    else
        command sudo $argv
    end
end
