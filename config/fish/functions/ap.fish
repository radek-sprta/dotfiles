function ap -d "Switch AWS profile"
    set -l profile (grep -Po '(?<=\[).+(?=\])' ~/.aws/credentials | fzf)
    set -xg AWS_PROFILE "$profile"
end
