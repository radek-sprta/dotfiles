# Defined in /tmp/fish.ZxsMkS/ddig.fish @ line 1
function ddig --description 'Get server name for domain'
	set ip_address (dig "$argv[1]" +short)
    dig -x "$ip_address" +short
end
