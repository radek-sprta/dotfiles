function fm --description 'Alias for file manager'
  if command -q yazi
    yazi $argv;
  else if command -q joshuto
    joshuto $argv;
  else
    ranger $argv;
  end
end
