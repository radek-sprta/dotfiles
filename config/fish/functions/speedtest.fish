function speedtest --description "Test the network speed"
    if test "$argv" = "10"
        wget -O /dev/null http://cachefly.cachefly.net/10mb.test
    else
        wget -O /dev/null http://cachefly.cachefly.net/100mb.test
    end
end
