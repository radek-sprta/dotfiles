function clean-disabled-snaps -d "Remove disabled snap packages"
    snap list --all | awk '/disabled/{printf "sudo snap remove %s --revision=%i \n",$1,$3}' | parallel
end
