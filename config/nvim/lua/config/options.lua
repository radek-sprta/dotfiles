-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here
--
-- Use :help 'option' to see the documentation for the given option.

-- Folding
vim.opt.foldlevelstart = 5 -- when there are few folds, expand them
vim.opt.foldnestmax = 2 -- don't go overboard with folds

-- Tab
vim.opt.shiftwidth = 0 -- set indent & unindent to the same as tabstop
vim.opt.softtabstop = 4 -- number of spaces in tab when editing
vim.opt.tabstop = 4 -- number of visual spaces per TAB

-- UI config
vim.g.snacks_animate = false -- turn off animations
vim.opt.colorcolumn = "80" -- highlight 80th column
vim.opt.list = false -- do not show whitespace
vim.opt.showmatch = true -- show matching parentheses

-- LSP
vim.g.ai_cmp = false -- Use inline suggestions for Copilot
