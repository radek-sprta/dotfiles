return {
	-- disabled
	{ "folke/tokyonight.nvim", enabled = false },
	{ "catpuccin/nvim", enabled = false },
	{ "lewis6991/gitsigns.nvim", enabled = false },
	-- Tmux navigation
	{
		"alexghergh/nvim-tmux-navigation",
		config = function()
			require("nvim-tmux-navigation").setup({
				disable_when_zoomed = true, -- defaults to false
				keybindings = {
					left = "<C-h>",
					down = "<C-j>",
					up = "<C-k>",
					right = "<C-l>",
					last_active = "<C-\\>",
					next = "<C-Space>",
				},
			})

			local function tmux_command(command)
				local tmux_socket = vim.fn.split(vim.env.TMUX, ",")[1]
				return vim.fn.system("tmux -S " .. tmux_socket .. " " .. command)
			end

			local nvim_tmux_nav_group = vim.api.nvim_create_augroup("NvimTmuxNavigation", {})

			vim.api.nvim_create_autocmd({ "VimEnter", "VimResume" }, {
				group = nvim_tmux_nav_group,
				callback = function()
					tmux_command("set-option -p @is_vim yes")
				end,
			})

			vim.api.nvim_create_autocmd({ "VimLeave", "VimSuspend" }, {
				group = nvim_tmux_nav_group,
				callback = function()
					tmux_command("set-option -p -u @is_vim")
				end,
			})
		end,
	},
	-- Copilot
	{
		"zbirenbaum/copilot.lua",
		cmd = "Copilot",
		build = ":Copilot auth",
		event = "InsertEnter",
		opts = {
			suggestion = {
				enabled = not vim.g.ai_cmp,
				auto_trigger = true,
				keymap = {
					accept = false, -- handled by nvim-cmp / blink.cmp
					next = "<M-]>",
					prev = "<M-[>",
				},
			},
			panel = { enabled = false },
			filetypes = {
				markdown = true,
				help = true,
				lua = true,
			},
		},
	},
	{ "giuxtaposition/blink-cmp-copilot" },
	{
		"zbirenbaum/copilot.lua",
		opts = function()
			LazyVim.cmp.actions.ai_accept = function()
				if require("copilot.suggestion").is_visible() then
					LazyVim.create_undo()
					require("copilot.suggestion").accept()
					return true
				end
			end
		end,
	},
	{
		"saghen/blink.cmp",
		dependencies = { "giuxtaposition/blink-cmp-copilot" },
		opts = {
			keymap = {
				preset = "super-tab",
				["<C-k>"] = { "select_prev", "fallback" },
				["<C-j>"] = { "select_next", "fallback" },
			},
			sources = {
				default = { "copilot" },
				providers = {
					copilot = {
						name = "copilot",
						module = "blink-cmp-copilot",
						kind = "Copilot",
						score_offset = 100,
						async = true,
					},
				},
			},
		},
	},
	-- LSP
	{
		"neovim/nvim-lspconfig",
		---@class PluginLspOpts
		opts = {
			---@type lspconfig.options
			servers = {
				bashls = {},
				dockerls = {},
				fish_lsp = {},
				gopls = {},
				jsonls = {},
				ruff = {},
				rust_analyzer = {},
				terraformls = {},
				yamlls = {},
			},
		},
	},
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, {
				"css",
				"desktop",
				"dockerfile",
				"fish",
				"git_config",
				"gitignore",
				"go",
				"hcl",
				"just",
				"rust",
				"terraform",
			})
		end,
	},
	-- Solarized colorscheme
	{
		"maxmx03/solarized.nvim",
		lazy = false,
		priority = 1000,
		---@type solarized.config
		opts = {},
		config = function(_, opts)
			vim.o.termguicolors = true
			vim.o.background = "dark"
			require("solarized").setup(opts)
		end,
	},
	{
		"LazyVim/LazyVim",
		opts = {
			colorscheme = "solarized",
		},
	},
}
